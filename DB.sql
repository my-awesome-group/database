-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 14 jan. 2020 à 09:39
-- Version du serveur :  10.3.17-MariaDB
-- Version de PHP :  7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `p1800666`
--

-- --------------------------------------------------------

--
-- Structure de la table `arbitre`
--

CREATE TABLE `arbitre` (
  `idArbitre` int(11) NOT NULL,
  `nom` varchar(32) DEFAULT NULL,
  `prenom` varchar(32) DEFAULT NULL,
  `nationalite` char(2) DEFAULT NULL,
  `categorie` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `arbitre`
--

INSERT INTO `arbitre` (`idArbitre`, `nom`, `prenom`, `nationalite`, `categorie`) VALUES
(1, 'Denlta', 'Alphonse', 'DJ', 'JAT2'),
(3, 'Do', 'Mika', 'BE', 'ITT1'),
(4, 'Ponsse', 'Pierre', 'HR', 'JAT2'),
(5, 'Frere', 'Jack', 'CH', 'JAT2'),
(6, 'Lepetit', 'Alexandre', 'RO', 'JAT2'),
(7, 'Pozission', 'Paul', 'RW', 'JAT2'),
(8, 'Oute', 'Albert', 'IN', 'JAT2'),
(9, 'Renne', 'Alexis', 'US', 'JAT2'),
(10, 'KiféYhan', 'Anne', 'CG', 'ITT1'),
(11, 'Téran', 'Hervé', 'CZ', 'JAT2'),
(44, 'Amai', 'George', 'UK', 'JAT2'),
(45, 'Dubois', 'Lucie', 'FR', 'JAT2'),
(46, 'Tinémilou', 'Martin', 'BE', 'JAT2'),
(47, 'Ulé', 'Frank', 'AT', 'ITT1'),
(48, 'Ehmorti', 'Emeric', 'US', 'JAT2'),
(49, 'Oukumidoya', 'Lise', 'JP', 'ITT1'),
(50, 'aouaboumchikiboum', 'Killian', 'KZ', 'JAT2'),
(51, 'Dejardin', 'Antonin', 'FR', 'JAT2'),
(52, 'Tardise', 'Yvan', 'IT', 'JAT2'),
(53, 'Attend', 'Charle', 'EE', 'JAT2'),
(54, 'Faire', 'Lucie ', 'UA', 'JAT2'),
(55, 'Osseroce', 'Laurine', 'BE', 'JAT2'),
(56, 'Pipourtoua', 'Nathan', 'SE', 'JAT2'),
(57, 'Eric', 'Alfred', 'DE', 'JAT2'),
(58, 'Teure', 'Armand', 'BY', 'ITT1'),
(59, 'Bash', 'Michel', 'UK', 'JAT2'),
(60, 'Eure', 'Calisse', 'AL', 'JAT2'),
(61, 'Ehdéfleche', 'Marc', 'KR', 'JAT2'),
(62, 'Stopie', 'Elodie', 'HU', 'JAT2'),
(63, 'Demousse', 'Robin', 'GR', 'JAT2'),
(64, 'Eha', 'Cedric', 'CZ', 'JAT2'),
(65, 'Alajoie', 'Aude', 'FR', 'JAT2'),
(66, 'Namare', 'Cristian', 'ES', 'JAT2'),
(67, 'Cantonner', 'Marie', 'CN', 'JAT2'),
(68, 'Drapa', 'Vivien', 'IT', 'JAT2'),
(69, 'Ih-De-Mace', 'Arthur', 'FR', 'JAT2'),
(70, 'Tusse', 'Eric', 'BR', 'JAT2'),
(71, 'Detrefle', 'Francis', 'IE', 'JAT2'),
(72, 'Ité', 'Patrick', 'AT', 'JAT2'),
(73, 'Oshild', 'Catherine', 'US', 'JAT2'),
(74, 'Etvire', 'Mikael', 'DZ', 'JAT2'),
(75, 'Malopiez', 'Roger', 'AR', 'JAT2'),
(76, 'Anlavalé', 'Mathilde', 'JP', 'JAT2'),
(77, 'Bienkeumal', 'Tristan', 'CU', 'JAT2'),
(78, 'Nage', 'André', 'DK', 'JAT2'),
(79, 'Madaire', 'Pedro', 'GN', 'JAT2'),
(80, 'Tancile', 'Marcus', 'HT', 'ITT1'),
(81, 'Tale', 'Florian', 'DZ', 'ITT1'),
(82, 'Douche', 'Angel', 'AI', 'ITT1');

-- --------------------------------------------------------

--
-- Structure de la table `court`
--

CREATE TABLE `court` (
  `idCourt` int(11) NOT NULL,
  `principal` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `court`
--

INSERT INTO `court` (`idCourt`, `principal`) VALUES
(1, 0),
(2, 0),
(3, 1),
(4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `duoJoueur`
--

CREATE TABLE `duoJoueur` (
  `idDuoJoueur` int(11) NOT NULL,
  `premierJoueur` int(11) DEFAULT NULL,
  `secondJoueur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `duoJoueur`
--

INSERT INTO `duoJoueur` (`idDuoJoueur`, `premierJoueur`, `secondJoueur`) VALUES
(1, 32, 15),
(2, 5, 48),
(3, 3, 42),
(4, 4, 26),
(5, 1, 12),
(6, 17, 21),
(7, 47, 43),
(8, 49, 34),
(9, 50, 14),
(10, 2, 19),
(11, 38, 56),
(12, 51, 36),
(13, 7, 54),
(14, 8, 55),
(15, 53, 6),
(16, 9, 25);

-- --------------------------------------------------------

--
-- Structure de la table `entrainement`
--

CREATE TABLE `entrainement` (
  `idEntrainement` int(11) NOT NULL,
  `idCourt` int(11) DEFAULT NULL,
  `idJoueur` int(11) DEFAULT NULL,
  `dateEntrainement` date DEFAULT NULL,
  `heureDebut` float DEFAULT NULL,
  `heureFin` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `equipeRamasseurs`
--

CREATE TABLE `equipeRamasseurs` (
  `idEquipeRamasseur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `equipeRamasseurs`
--

INSERT INTO `equipeRamasseurs` (`idEquipeRamasseur`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--

CREATE TABLE `joueur` (
  `idJoueur` int(11) NOT NULL,
  `nom` varchar(32) DEFAULT NULL,
  `prenom` varchar(32) DEFAULT NULL,
  `atp` int(11) DEFAULT NULL,
  `nationalite` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `joueur`
--

INSERT INTO `joueur` (`idJoueur`, `nom`, `prenom`, `atp`, `nationalite`) VALUES
(1, 'Nadal', 'Rafael', 1, 'ES'),
(2, 'Djokovic', 'Novak', 2, 'RS'),
(3, 'Federer', 'Roger', 3, 'CH'),
(4, 'Molleker', 'Rudolf', 163, 'DE'),
(5, 'Pospisil', 'Vasek', 148, 'CA'),
(6, 'Bagnis', 'Facundo', 135, 'GR'),
(7, 'Karlovic', 'Ivo', 123, 'HR'),
(8, 'Kudla', 'Denis', 115, 'US'),
(9, 'Veselý', 'Jirí', 102, 'CZ'),
(10, 'Harris', 'Lloyd', 99, 'ZA'),
(11, 'Džumhur', 'Damir', 91, 'BA'),
(12, 'Munar', 'Jaume', 89, 'ES'),
(13, 'Moutet', 'Corentin', 81, 'FR'),
(14, 'Nishioka', 'Yoshihito', 72, 'JP'),
(15, 'Thompson', 'Jordan', 63, 'AU'),
(16, 'Sousa', 'João', 59, 'PT'),
(17, 'Gasquet', 'Richard', 60, 'FR'),
(18, 'Basilashvili', 'Nikoloz', 26, 'GE'),
(19, 'Krajinovic', 'Filip', 40, 'RS'),
(20, 'Cuevas', 'Pablo', 45, 'UY'),
(21, 'Simon', 'Gilles', 57, 'FR'),
(22, 'Bublik', 'Alexander', 55, 'KZ'),
(23, 'Monteiro', 'Thiago', 87, 'BR'),
(24, 'Kwon', 'Soonwoo', 86, 'KR'),
(25, 'Li', 'Zhe', 209, 'CN'),
(26, 'Brown', 'Dustin', 203, 'DE'),
(28, 'Griekspoor', 'Tallon', 175, 'NL'),
(29, 'Stakhovsky', 'Sergiy', 153, 'UA'),
(30, 'Torpegaard', 'Mikael', 166, 'DK'),
(31, 'Copil', 'Marius', 152, 'RO'),
(32, 'O\'Connell', 'Christopher', 117, 'AU'),
(33, 'Ymer', 'Mikael', 76, 'SE'),
(34, 'Caruso', 'Salvatore', 93, 'IT'),
(35, 'Berankis', 'Ricardas', 67, 'LT'),
(36, 'Evans', 'Daniel', 42, 'UK'),
(37, 'Garín', 'Cristian', 33, 'CL'),
(38, 'Thiem', 'Dominic', 4, 'AT'),
(39, 'Varillas', 'Juan Pablo', 142, 'PE'),
(40, 'Sela', 'Dudi', 141, 'IL'),
(41, 'Ivashka', 'Ilya', 136, 'BY'),
(42, 'Laaksonen', 'Henri', 101, 'CH'),
(43, 'Herbert', 'Pierre-Hugues', 65, 'FR'),
(44, 'Hurkacz', 'Hubert', 37, 'PL'),
(45, 'Khachanov', 'Karen', 17, 'RU'),
(46, 'Goffin', 'David', 11, 'BE'),
(47, 'Monfils', 'Gaël', 9, 'FR'),
(48, 'Auger-Aliassime', 'Félix', 21, 'CA'),
(49, 'Travaglia', 'Stefano', 82, 'IT'),
(50, 'Nishikori', 'Kei', 16, 'JP'),
(51, 'Edmund', 'Kyle', 69, 'UK'),
(52, 'Seppi', 'Andreas', 71, 'IT'),
(53, 'Tsitsipas', 'Stefanos', 6, 'GR'),
(54, 'Cilic', 'Marin', 38, 'HR'),
(55, 'Opelka', 'Reilly', 36, 'US'),
(56, 'Novak', 'Dennis', 105, 'AT');

-- --------------------------------------------------------

--
-- Structure de la table `matchArbitreLigne`
--

CREATE TABLE `matchArbitreLigne` (
  `idMatch` int(11) NOT NULL,
  `idArbitre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matchArbitreLigne`
--

INSERT INTO `matchArbitreLigne` (`idMatch`, `idArbitre`) VALUES
(1, 6),
(1, 8),
(1, 45),
(1, 51),
(1, 59),
(1, 62),
(1, 74),
(1, 77),
(2, 4),
(2, 7),
(2, 11),
(2, 57),
(2, 60),
(2, 64),
(2, 73),
(2, 79);

-- --------------------------------------------------------

--
-- Structure de la table `matchDuoJoueur`
--

CREATE TABLE `matchDuoJoueur` (
  `idMatch` int(11) NOT NULL,
  `premierDuo` int(11) DEFAULT NULL,
  `secondDuo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matchDuoJoueur`
--

INSERT INTO `matchDuoJoueur` (`idMatch`, `premierDuo`, `secondDuo`) VALUES
(2, 12, 9);

-- --------------------------------------------------------

--
-- Structure de la table `matchEquipeRamasseurs`
--

CREATE TABLE `matchEquipeRamasseurs` (
  `idMatch` int(11) NOT NULL,
  `idEquipeRamasseur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matchEquipeRamasseurs`
--

INSERT INTO `matchEquipeRamasseurs` (`idMatch`, `idEquipeRamasseur`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `matchJoueur`
--

CREATE TABLE `matchJoueur` (
  `idMatch` int(11) NOT NULL,
  `premierJoueur` int(11) DEFAULT NULL,
  `secondJoueur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matchJoueur`
--

INSERT INTO `matchJoueur` (`idMatch`, `premierJoueur`, `secondJoueur`) VALUES
(1, 12, 42);

-- --------------------------------------------------------

--
-- Structure de la table `matchSet`
--

CREATE TABLE `matchSet` (
  `idMatch` int(11) NOT NULL,
  `indexSet` tinyint(3) NOT NULL,
  `scorePremier` tinyint(7) DEFAULT NULL,
  `scoreSecond` tinyint(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matchSet`
--

INSERT INTO `matchSet` (`idMatch`, `indexSet`, `scorePremier`, `scoreSecond`) VALUES
(1, 1, 6, 3),
(1, 2, 6, 4),
(2, 1, 6, 3),
(2, 2, 4, 6),
(2, 3, 2, 6);

-- --------------------------------------------------------

--
-- Structure de la table `matchT`
--

CREATE TABLE `matchT` (
  `idMatch` int(11) NOT NULL,
  `duo` tinyint(1) DEFAULT NULL,
  `dateMatch` date DEFAULT NULL,
  `horaire` int(11) DEFAULT NULL,
  `indexGagnant` tinyint(1) DEFAULT NULL,
  `idCourt` int(11) DEFAULT NULL,
  `idArbitreChaise` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matchT`
--

INSERT INTO `matchT` (`idMatch`, `duo`, `dateMatch`, `horaire`, `indexGagnant`, `idCourt`, `idArbitreChaise`) VALUES
(1, 0, '2020-05-20', 1, 1, 1, 58),
(2, 1, '2020-05-20', 1, 2, 4, 82);

-- --------------------------------------------------------

--
-- Structure de la table `ramasseur`
--

CREATE TABLE `ramasseur` (
  `idRamasseur` int(11) NOT NULL,
  `idEquipeRamasseur` int(11) DEFAULT NULL,
  `nom` varchar(32) DEFAULT NULL,
  `prenom` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ramasseur`
--

INSERT INTO `ramasseur` (`idRamasseur`, `idEquipeRamasseur`, `nom`, `prenom`) VALUES
(1, 1, 'Cascio Ferro', 'Vito'),
(2, 1, 'Zoupa', 'Abdel'),
(3, 1, 'Alle', 'Gustave'),
(4, 1, 'Eularepublique', 'Gertrude'),
(5, 1, 'Etoui', 'Manon'),
(6, 1, 'Tarantino', 'Quentin'),
(7, 1, 'Bon', 'Jean'),
(8, 1, 'Ettes', 'Rose'),
(9, 2, 'Tamar', 'Justin'),
(10, 2, 'Abelle', 'Johane'),
(11, 2, 'Tamé', 'Emré'),
(12, 2, 'Teufarci', 'Thomas'),
(13, 2, 'Eration', 'Judith'),
(14, 2, 'Tancile', 'Marcus'),
(15, 2, 'Béh', 'Gaston'),
(16, 2, 'Nou', 'Marion'),
(17, 3, 'Uhbeurdenlézepinar', 'Mohamed'),
(18, 3, 'Téhenbas', 'Raymond'),
(19, 3, 'Avoué', 'Justine'),
(20, 3, 'Aculé', 'Maxime'),
(21, 3, 'Icole', 'Kevin'),
(22, 3, 'Etlaudora', 'Louis'),
(23, 3, 'Mouassa', 'Bastien'),
(24, 3, 'Vabo', 'Nicolas'),
(25, 4, 'Yquelqun', 'Theo'),
(26, 4, 'Davinsonne', 'Charlay'),
(27, 4, 'Sert', 'Jules'),
(28, 4, 'Ide', 'Victor'),
(29, 4, 'Pard', 'Leo'),
(30, 4, 'Etpoivre', 'Marcel'),
(31, 4, 'Unlivre', 'Ophelie'),
(32, 4, 'Ticho', 'Bernard');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `arbitre`
--
ALTER TABLE `arbitre`
  ADD PRIMARY KEY (`idArbitre`);

--
-- Index pour la table `court`
--
ALTER TABLE `court`
  ADD PRIMARY KEY (`idCourt`);

--
-- Index pour la table `duoJoueur`
--
ALTER TABLE `duoJoueur`
  ADD PRIMARY KEY (`idDuoJoueur`);

--
-- Index pour la table `entrainement`
--
ALTER TABLE `entrainement`
  ADD PRIMARY KEY (`idEntrainement`);

--
-- Index pour la table `equipeRamasseurs`
--
ALTER TABLE `equipeRamasseurs`
  ADD PRIMARY KEY (`idEquipeRamasseur`);

--
-- Index pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD PRIMARY KEY (`idJoueur`);

--
-- Index pour la table `matchArbitreLigne`
--
ALTER TABLE `matchArbitreLigne`
  ADD PRIMARY KEY (`idMatch`,`idArbitre`);

--
-- Index pour la table `matchDuoJoueur`
--
ALTER TABLE `matchDuoJoueur`
  ADD PRIMARY KEY (`idMatch`);

--
-- Index pour la table `matchEquipeRamasseurs`
--
ALTER TABLE `matchEquipeRamasseurs`
  ADD PRIMARY KEY (`idMatch`,`idEquipeRamasseur`);

--
-- Index pour la table `matchJoueur`
--
ALTER TABLE `matchJoueur`
  ADD PRIMARY KEY (`idMatch`);

--
-- Index pour la table `matchSet`
--
ALTER TABLE `matchSet`
  ADD PRIMARY KEY (`idMatch`,`indexSet`);

--
-- Index pour la table `matchT`
--
ALTER TABLE `matchT`
  ADD PRIMARY KEY (`idMatch`);

--
-- Index pour la table `ramasseur`
--
ALTER TABLE `ramasseur`
  ADD PRIMARY KEY (`idRamasseur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `arbitre`
--
ALTER TABLE `arbitre`
  MODIFY `idArbitre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT pour la table `court`
--
ALTER TABLE `court`
  MODIFY `idCourt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `duoJoueur`
--
ALTER TABLE `duoJoueur`
  MODIFY `idDuoJoueur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `entrainement`
--
ALTER TABLE `entrainement`
  MODIFY `idEntrainement` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `equipeRamasseurs`
--
ALTER TABLE `equipeRamasseurs`
  MODIFY `idEquipeRamasseur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `joueur`
--
ALTER TABLE `joueur`
  MODIFY `idJoueur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT pour la table `matchT`
--
ALTER TABLE `matchT`
  MODIFY `idMatch` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `ramasseur`
--
ALTER TABLE `ramasseur`
  MODIFY `idRamasseur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
