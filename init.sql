DROP TABLE IF EXISTS ramasseur;
DROP TABLE IF EXISTS matchDuoJoueur;
DROP TABLE IF EXISTS matchJoueur;
DROP TABLE IF EXISTS matchEquipeRamasseurs;
DROP TABLE IF EXISTS matchArbitreLigne;
DROP TABLE IF EXISTS matchSet;
DROP TABLE IF EXISTS matchT;
DROP TABLE IF EXISTS equipeRamasseurs;
DROP TABLE IF EXISTS entrainement;
DROP TABLE IF EXISTS duoJoueur;
DROP TABLE IF EXISTS joueur;
DROP TABLE IF EXISTS court;
DROP TABLE IF EXISTS arbitre;

CREATE TABLE arbitre (
	idArbitre INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(32),
	prenom VARCHAR(32),
	nationalite CHAR(2),
	categorie VARCHAR(6)
);

CREATE TABLE court (
	idCourt INTEGER PRIMARY KEY AUTO_INCREMENT,
	principal BOOLEAN
);

CREATE TABLE joueur (
	idJoueur INTEGER PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(32),
	prenom VARCHAR(32),
	atp INTEGER,
	nationalite CHAR(2)
);

CREATE TABLE duoJoueur (
	idDuoJoueur INTEGER PRIMARY KEY AUTO_INCREMENT,
	premierJoueur INTEGER REFERENCES joueur(idJoueur),
	secondJoueur INTEGER REFERENCES joueur(idJoueur)
);

CREATE TABLE entrainement (
	idEntrainement INTEGER PRIMARY KEY AUTO_INCREMENT,
	idCourt INTEGER REFERENCES court(idCourt),
	idJoueur INTEGER REFERENCES joueur(idJoueur),
	dateEntrainement DATE,
	heureDebut FLOAT,
	heureFin FLOAT
);

CREATE TABLE equipeRamasseurs (
	idEquipeRamasseur INTEGER PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE matchT (
	idMatch INTEGER PRIMARY KEY AUTO_INCREMENT,
	duo BOOLEAN,
	dateMatch DATE,
	horaire INTEGER,
	indexGagnant TINYINT(1),
	idCourt INTEGER REFERENCES court(idCourt),
	idArbitreChaise INTEGER REFERENCES arbitre(idArbitre)
);

CREATE TABLE matchSet (
	idMatch INTEGER REFERENCES matchT(idMatch),
	indexSet TINYINT(3),
	scorePremier TINYINT(7),
	scoreSecond TINYINT(7),
	PRIMARY KEY(idMatch, indexSet)
);

CREATE TABLE matchArbitreLigne (
	idMatch INTEGER REFERENCES matchT(idMatch),
	idArbitre INTEGER REFERENCES arbitre(idArbitre),
	PRIMARY KEY(idMatch, idArbitre)
);

CREATE TABLE matchEquipeRamasseurs (
	idMatch INTEGER REFERENCES matchT(idMatch),
	idEquipeRamasseur INTEGER REFERENCES equipeRamasseurs(idEquipeRamasseur),
	PRIMARY KEY(idMatch, idEquipeRamasseur)
);

CREATE TABLE matchJoueur (
	idMatch INTEGER PRIMARY KEY REFERENCES matchT(idMatch),
	premierJoueur INTEGER REFERENCES joueur(idJoueur),
	secondJoueur INTEGER REFERENCES joueur(idJoueur)
);

CREATE TABLE matchDuoJoueur (
	idMatch INTEGER PRIMARY KEY REFERENCES matchT(idMatch),
	premierDuo INTEGER REFERENCES duoJoueur(idDuoJoueur),
	secondDuo INTEGER REFERENCES duoJoueur(idDuoJoueur)
);

CREATE TABLE ramasseur (
	idRamasseur INTEGER PRIMARY KEY AUTO_INCREMENT,
	idEquipeRamasseur INTEGER REFERENCES equipeRamasseurs(idEquipeRamasseur),
	nom VARCHAR(32),
	prenom VARCHAR(32)
);
